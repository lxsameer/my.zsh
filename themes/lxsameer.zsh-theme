# lxsameer.zsh-theme
#
# Author: @lxsameer <lxsameer@gnu.org>
# Repo: https://github.com/lxsameer/My.Zsh
#
# Created on:		Apr 10, 2012

function my_git_prompt_info() {
  ref=$(git symbolic-ref HEAD 2> /dev/null) || \
  ref=$(git rev-parse --short HEAD 2> /dev/null) || return
  echo "$(my_commits) $(count_commits) $ZSH_THEME_GIT_PROMPT_PREFIX %F{007}$(parse_git_dirty)%F{005}${ref#refs/heads/} %F{007}\uf126 $ZSH_THEME_GIT_PROMPT_SUFFIX"
}

function count_commits () {
    count="$(git rev-list --all --count)"
    formatted="$(printf "%05d" $count)"
    echo "%F{008}$formatted \uf02c"
}

function my_commits () {
    commits="$(git shortlog -s -n|grep 'Sameer Rahmani'|cut -f 1)"
    result="${commits// /}"
    formatted="$(printf "%04d" $result)"
    echo "%F{002}$formatted \uf1ae"
}
function get_pwd() {
   echo "${PWD/$HOME/~}"
}
function spaces() {
    local git=$(my_git_prompt_info)

    if [ ${#git} != 0 ]; then
	local clean=$(parse_git_dirty)
	if [ ${#clean} != 0 ]; then
	    (( git = ${#git} - 48 - ${#clean} + 1 ))
	else
	    (( git = ${#git} - 48 ))
	fi
    else
	git=0
    fi

    local termwidth
    (( termwidth = ${COLUMNS} - 18 - ${#$(get_pwd)} - ${git} ))

    local spacing=" "
    for i in {1..$termwidth}; do
	spacing="${spacing}-"
    done
    echo $spacing
}
function user_sign () {
    if [ $UID != 0 ]; then
	echo "%F{002}\uf058"
    else
	echo "%F{001}\uf071"
    fi
}

function path_icon () {
    echo "%F{004}\uf0c1"
}

function code_icon () {
    echo "%F{007}\uf121  "
}

function last_command_result () {
    echo "%F{005}%? \uf128"
}

function num_jobs () {
    echo "%F{004}%j \uf013"
}
PROMPT='
 $(user_sign) %F{007}[ $(last_command_result) $(num_jobs) %F{007}] %F{003}%~%F{008}$(spaces) $(my_git_prompt_info)
 $(code_icon)%f'

# git settings
ZSH_THEME_GIT_PROMPT_PREFIX="%F{002}["
ZSH_THEME_GIT_PROMPT_CLEAN=""
ZSH_THEME_GIT_PROMPT_DIRTY="%F{003}*"
ZSH_THEME_GIT_PROMPT_SUFFIX="%F{002}]"

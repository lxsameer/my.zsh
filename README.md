MyZsh
=====

My Zsh framework and configuration. It's minimal, simple and fast in compare to other frameworks. It designed
for a lazy person like myself. :P

## Installation
Use the following command to install **My.Zsh**:

```bash
sh -c "$(curl -fsSL https://gitlab.com/lxsameer/my.zsh/raw/master/scripts/install.sh)"
```

In order to update your copy just use `git pull` to get the latest changes.

## Configuration
The default configuration file is `~/.zshrc` but you should use `~/.zshrc.override` file
in order to store you custom configurations.
